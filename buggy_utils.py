# coding: utf-8


import argparse
from random import randrange
from inspect import getmembers, isfunction
import buggy_operations
from buggy_operations import *


# Gère les arguments
parser = argparse.ArgumentParser(
    description='Buggy - TP SLR EIAH - Romain Poupin')
parser.add_argument('-v', action="store_true",
                    default=False, help='active le mode verbose')
parser.add_argument('-l', action="store_true",
                    default=False, help='liste les bugs du module buggy_operations')
parser.add_argument('-n', type=int, default=3,
                    help='nombre de résulats à deviner')


MIN = 1
MAX = 999
VERBOSE = parser.parse_args().v
PRINT_BUGS = parser.parse_args().l
NB_DEVINER = parser.parse_args().n


# Affiche la trace msg si le mode verbose est actif


def verbose(msg):
    if VERBOSE:
        print(">>>", msg)


# retourne toutes les opérations du module buggy_operations


def get_all_operations():
    return [op for op in getmembers(buggy_operations) if isfunction(op[1])]


# retourne tous les bugs du modules buggy_operations


def get_bugs():
    return [op for op in get_all_operations() if "bug" in op[0]]


# Retourne les fonctions des opérations élémentaires
# en introduisant une version erronée choisit aléatoirement
#
# Le format de retour est un dictionnaire : {'nom_operation', <fonction>, ...}
# Les opérations sont automatiquement extraites du module buggy_operations


def init_operations():
    all = get_all_operations()
    bugs = get_bugs()
    bug = bugs[randrange(0, len(bugs))]
    ops = {}
    for op in all:
        if not "bug" in op[0]:
            if not op[0] in bug[0]:
                ops[op[0]] = op[1]
            else:
                ops[op[0]] = bug[1]
    verbose("opérations avec un bug : {}".format(ops))
    return ops, bug[0]


# Découpe les opérandes en tableaux
#
# Exemple : a = 123, b = 45
# ==> [3, 2, 1], [5, 4, 0]


def format_operandes(a, b):
    a = list(map(int, str(a)))
    b = list(map(int, str(b)))
    a.reverse()
    b.reverse()
    for i in range(len(b), len(a)):
        b.append(0)
    verbose("opérandes : a = {}, b = {}".format(a, b))
    return a, b


# Restitue l'entier correspondant au tableau result
#
# Exemple : result = [8, 7, 0]
# ==> 78


def format_result(result):
    verbose("result : {}".format(result))
    result.reverse()
    result = [str(int) for int in result]
    result = "".join(result)
    return int(result)
