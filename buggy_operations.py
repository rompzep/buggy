# coding: utf-8


# Opérations correctes


# Définit quel opérande est soustrait à l'autre.
#
# Le premier retourné est le soustrait.
# Le second est le soustracteur


def set_operandes(a, b):
    return a, b


# Calcule la retenue


def set_retenue(a, b):
    if a < b:
        return 10
    return 0


# Calcule le report


def set_report(retenue):
    if retenue == 10:
        return 1
    return 0


# Applique la retenue au soustrait


def apply_retenue(a, retenue):
    return a + retenue


# Applique le report au soustracteur


def apply_report(b, report):
    return b + report


# Calcule la soustraction


def soustrait(a, b):
    return a - b


# Variantes incorrectes des opérations
#
# Ajoutez ici vos bugs à la volée.
# Syntaxe : <nom_operation>_bug_<nom_bug>


# Affecte le terme soustrait par le maximum
# des deux et le soustracteur par l'autre


def set_operandes_bug_max(a, b):
    return max(a, b), min(a, b)


# Oublie d'appliquer la retenue


def apply_retenue_bug_oublier(a, retenue):
    return a


# Oublie d'appliquer le report


def apply_report_bug_oublier(b, report):
    return b
