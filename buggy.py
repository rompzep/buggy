# coding: utf-8


# Lancement : python3 buggy.py
# -v pour verbose
# -d <int> pour le nombre de résultats à deviner


from buggy_utils import *


# L'utilisateur propose à Buggy de résoudre
# autant de soustractions qu'il souhaite


def proposer_soustractions(ops):
    stop = "n"
    while stop == "n":
        a = int(input("a = "))
        b = int(input("b = "))
        if a < 0 or b < 0:
            print("nombres négatifs interdits")
            continue
        elif b > a:
            print("b doit être inférieur ou égal à a")
            continue
        else:
            print("==>", soustraction(a, b, ops))

        stop = ""
        while stop != "o" and stop != "n":
            stop = input("Pensez-vous avoir deviné le bug ? [o/n] ")


# Buggy demande à l'utilisateur de deviner le résultat
# que produirait le bug pour une succession de soustractions
#
# Renvoie True s'il répond correctement à toutes


def deviner_resultats(ops):
    res = []
    user = []
    for i in range(NB_DEVINER):
        numbers = [randrange(MIN, MAX), randrange(MIN, MAX)]
        a = max(numbers)
        b = min(numbers)
        res.append(soustraction(a, b, ops))
        user.append(int(input("a = {}\nb = {}\n==> ".format(a, b))))

    verdict = True
    for i in range(NB_DEVINER):
        valid = res[i] == user[i]
        print("Soustraction {} : {}".format(i + 1, "OK" if valid else "KO"))
        if not valid:
            verdict = False
    return verdict


# La soustraction est réalisée en appliquant la succession d'opérations
# sur chaque itération des colonnes des opérandes
#
# Exemple : a = 123, b = 45
#
# itération 1:
#   _a = 3, retenue = 10, _b = 5, report = 0
#   3+10 - 5+0 = 8
#   ==> res = [8]
#
# itération 2:
#   _a = 2, retenue = 10, _b = 4, report = 1
#   2+10 - 4+1 = 7
#   ==> res = [8, 7]
#
# itération 3:
#   _a = 1, retenue = 0, _b = 0, report = 1
#   1+0 - 0+1 = 0
#   ==> res = [8, 7, 0]
#
# 123 - 45 = 78


def soustraction(a, b, ops):
    a, b = format_operandes(a, b)
    res = []
    report = 0
    for i in range(0, len(a)):
        _a, _b = ops['set_operandes'](a[i], b[i])
        verbose("définit les opérandes\t: {} - {}".format(_a, _b))
        _b = ops['apply_report'](_b, report)
        verbose("applique le report\t\t: {} - {}".format(_a, _b))
        retenue = ops['set_retenue'](_a, _b)
        verbose("calcule la retenue\t\t: {}".format(retenue))
        report = ops['set_report'](retenue)
        verbose("calcule le prochain report\t: {}".format(report))
        _a = ops['apply_retenue'](_a, retenue)
        verbose("applique la retenue\t\t: {} - {}".format(_a, _b))
        # valeur absolue pour se prévenir des bugs qui produisent des résulats négatifs
        _res = abs(ops['soustrait'](_a, _b))
        verbose("résultat intermédiaire\t: {}".format(_res))
        res.append(_res)
    return format_result(res)


def main():
    if PRINT_BUGS:
        for bug in get_bugs():
            print(bug[0])
    else:
        ops, bug = init_operations()
        print("J'ai choisi un bug, proposez-moi des soustractions.")
        proposer_soustractions(ops)
        print("Devinez le résultat que produirait le bug pour les soustractions suivantes.")
        while not deviner_resultats(ops):
            print(
                "Vous n'avez pas réussi à deviner les résultats, proposez-moi à nouveau des soustractions.")
            proposer_soustractions(ops)
        print("Félicitations ! Vous avez réussi à deviner les résultats.")
        print("L'opération buggée était :", bug)


# Éxécute la fonction main au lancement de buggy
if __name__ == '__main__':
    main()
